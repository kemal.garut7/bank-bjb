# No 1
Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)

![pendekatan-matematika](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/aritmatika.gif)

PENJELASAN :
System matematika ini ada pada class transfer, terdapat form nominal, disaat user berhasil melakukan transfer dengan melewati keamanan form yang ada, maka "Saldo" akan dikurangi nominal serta "Pengeluaran" akan ditambah dengan nominal dan langsung di update ke database dan tampilan untuk saldo akan terupdate dengan sign out dulu lalu masuk kembali.

# No 2
Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

![solusi-algoritma](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/algoritma_login.gif)

PENJELASAN:
##### 1. Algoritma Login dengan Penyimpanan Kata Sandi (Password-based Login)
Algoritma ini berfungsi sebagai Sistem perbandingan kata sandi yang dimasukkan, dengan password yang disimpan dalam database MySql. Jika password cocok, pengguna dianggap berhasil login.

##### 2. Algoritma Linear Search
Algoritma pencarian ini digunakan pada fitur "Pencarian transaksi" di mana setiap elemen/huruf dalam database secara bergantian diperiksa sampai elemen/huruf yang dicari ditemukan, serta dalam pencariannya berbasis realtime/live dimana user tidak perlu mengklik enter atau klik button untuk eksekusi inputan yang dicari, keyword yang bisa di cari antara lain nama pemilik rekening, no rekening, tanggal transfer dan status transaksi.

# No 3
Mampu menjelaskan konsep dasar OOP

![konsep-dasar-OOP](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/konsep_dasar_OOP.gif)

PENJELASAN:
Ada beberapa konsep dasar dalam OOP, di antaranya:

    1.Class: sebuah blueprint untuk membuat objek. Di dalam class dapat menentukan sifat dan perilaku dari objek yang akan dibuat. Penamaan class biasanya di awali dengan kapital.

    2.Objek: sebuah instansiasi dari class yang memiliki sifat dan perilaku yang telah didefinisikan di dalam class.

    3.Encapsulation: konsep di mana sifat dan perilaku objek dibungkus atau dibatasi aksesnya melalui metode atau fungsi yang telah didefinisikan di dalam class. Hal ini dilakukan untuk memastikan integritas data dan mencegah perubahan yang tidak diinginkan.

    4.Inheritance: konsep di mana sebuah class dapat mewarisi sifat dan perilaku dari class induk. Hal ini memungkinkan untuk membuat class baru yang memiliki sifat dan perilaku yang mirip dengan class yang sudah ada.

    5.Polymorphism: konsep di mana objek dapat memiliki banyak bentuk atau perilaku yang berbeda tergantung pada konteks penggunaannya. Hal ini memungkinkan untuk membuat kode yang lebih fleksibel dan mudah diubah.

    6. Abstraction: konsep untuk menyembunyikan detail implementasi suatu objek dan hanya mengekspos fungsionalitas penting yang dibutuhkan. Dalam abstraksi, kita hanya mempertimbangkan fitur atau perilaku objek yang relevan dan mempertimbangkan mereka sebagai satu entitas yang terpisah dari detail implementasi internalnya.

# No 4
Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

![encapsulation](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/encapsulation_Data.gif)

PENJELASAN:

##### Encapsulation 
>ada pada class "DataRekening", berfungsi untuk mengembalikan nilai secara pasti hal itu di kirim datanya melalui setter dan di tangkap oleh getter. Mengapa harus pada class ini, karena class ini memiliki fungsi yang berkaitan erat dengan database dalam pengiriman data juga mencegah perubahan data secara tidak sengaja.

# No 5
Mampu mendemonstrasikan penggunaan Abstraction secara tepat  (Lampirkan link source code terkait)

![abstraction](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/abstract_javascript.gif)

PENJELASAN:

##### Abstraction 
>ada pada class "BankInput", karena pada class tersebut terjadinya pemilihan list bank. Kegunaan Abstraction ialah untuk menyembunyikan fungsionalitas penting saja dan tidak harus di implementasikan. Dilakukan implementasinya pada class lain yang ter extends pada class abstract. Class "BankInput" itu ialah class induk dari subclass yang mengimplementasikan method abstractnya, apa yang di implementasikan?, yaitu nama-nama bank.

# No 6
Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

![inheritance-polymorphism](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/inheritance_polymorphism.gif)

PENJELASAN:
##### Inheritance 
>ada pada class "BankInput", kegunaan class itu ialah menampilkan pilihan bank, tetapi class tersebut abstract jadi belum di implementasikan. Dibuat lah subclass nya yaitu "BankInputPilih" yang mewarisi "BankInput". Dalam subclass tersebut dibuat struktur dan komponen yang berfungsi menampilkan list bank yang sebenarnya dengan memanggil method abstractnya.

##### Polymorphism
>Override: ada pada class "Wallet" yang sudah mewarisi class "DataRekening", dimana method dari class "DataRekening" di panggil pada class "Wallet" yang gunanya untuk pengiriman data nomor rekening dari database, jadi selain user bisa transfer dengan nomor hp wallet tujuan, user juga bisa menginput nomor rekening yang sudah terintegrasi dengan wallet tertentu.
>Overload: ada pada class "PencarianHistory" dimana method yang namanya sama namun berbeda dalam parameter. Kegunaannya disini saya untuk meminimalisir error, jadi disaat method yang tanpa parameter error, akan di backup dengan method yang memiliki parameter begitupun sebaliknya.



# No 7
Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

##### UseCase: Login

    1. User masuk tampilan login
    2. User memasukkan username
    3. User memasukkan password
    4. User mengklik tombol login
    5. User berhasil login
    6. Sistem menampilkan halaman home



##### UseCase: BankInput

    1. User memilih menu "Transfer Bank" di side bar
    2. User memilih bank tujuan
    3. User memasukan nomor rekening tujuan
    4. System langsung mengecek no rekening apakah ada dalam database
    5. Jika ada, maka muncul nama pemilik rekening
    6. User bisa mengklik tombol "Transfer"
    7. Transaksi akan masuk ke "History Transaksi"



##### UseCase: Wallet


    1. User memilih menu "E-Wallet" di side bar
    2. User memilih E-Wallet tujuan
    3. User memasukan nomor rekening/nomor hp tujuan
    4. System langsung mengecek rekening/nomor hp apakah ada dalam database
    6. Jika ada, maka muncul nama pemilik rekening
    7. Jika ada user bisa mengklik tombol "Kirim"
    8. Transaksi akan masuk ke "History Transaksi"


##### UseCase: SettingModal


    1. User memilih menu "Pengaturan" di side bar
    2. User memasukan password
    3. Jika benar, User masuk ke halaman Pengaturan
    4. User bisa melihat data pribadi
    


##### UseCase: Logout

    User memilih menu "Keluar" di side bar

# No 8
Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

#### Class Diagram
![Class Diagram](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/class_diagram.drawio.png)


#### Use Case Table
| Key  | Use Case | Priority | Status |
| ------------- | ------------- |------------- |------------- |
| 1  | User ingin bisa login dengan akun pribadi | Tinggi | Completed |
| 2 | User ingin bisa transfer antar bank ke sesama bank  | Tinggi | Completed |
| 3 | User ingin bisa transfer antar bank ke beda bank  | Tinggi | Planned |
| 4 | User ingin bisa transfer wallet ke akun sesama wallet | Tinggi | Planned |
| 5 | User ingin bisa transfer wallet ke akun wallet yang berbeda  | Tinggi | Planned |
| 6 | User ingin bisa mengecek informasi tentang dirinya | Tinggi | Completed |
| 7 | User ingin bisa mengisi pulsa | Rendah | Unplanned |
| 8 | User ingin bisa membayar tagihan listrik | sedang | Planned |

#### Use Case Diagram
![Use Case Diagram](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/use_case_bjb.drawio.png)


# No 9
Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

![Video Demo](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/Demo_BJB.mp4)
#### Youtube 
[Youtube Kemal](https://www.youtube.com/watch?v=2aD6BtwpgEE)

# No 10
Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

![inovasi-UX](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/UX.gif)

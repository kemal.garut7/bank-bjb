# Video Demo Project

![Video Demo](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/Demo_BJB.mp4)



# Use Case 

##### UseCase: Login

    1. User masuk tampilan login
    2. User memasukkan username
    3. User memasukkan password
    4. User mengklik tombol login
    5. User berhasil login
    6. Sistem menampilkan halaman home



##### UseCase: Transfer

    1. User memilih menu "Transfer Bank" di side bar
    2. User memilih bank tujuan
    3. User memasukan nomor rekening tujuan
    4. System langsung mengecek no rekening apakah ada dalam database
    5. Jika ada, maka muncul nama pemilik rekening
    6. User bisa mengklik tombol "Transfer"
    7. Transaksi akan masuk ke "History Transaksi"



##### UseCase: Wallet


    1. User memilih menu "E-Wallet" di side bar
    2. User memilih E-Wallet tujuan
    3. User memasukan nomor rekening/nomor hp tujuan
    4. System langsung mengecek rekening/nomor hp apakah ada dalam database
    6. Jika ada, maka muncul nama pemilik rekening
    7. Jika ada user bisa mengklik tombol "Kirim"
    8. Transaksi akan masuk ke "History Transaksi"


##### UseCase: Pengaturan


    1. User memilih menu "Pengaturan" di side bar
    2. User memasukan password
    3. Jika benar, User masuk ke halaman Pengaturan
    4. User bisa melihat data pribadi
    


##### UseCase: Logout

    User memilih menu "Keluar" di side bar


# Class Diagram
![Class Diagram](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/class_diagram.drawio.png)


# Use Case Table
| Key  | Use Case | Priority | Status |
| ------------- | ------------- |------------- |------------- |
| 1  | User ingin bisa login dengan akun pribadi | Tinggi | Completed |
| 2 | User ingin bisa transfer antar bank ke sesama bank  | Tinggi | Completed |
| 3 | User ingin bisa transfer antar bank ke beda bank  | Tinggi | Planned |
| 4 | User ingin bisa transfer wallet ke akun sesama wallet | Tinggi | Planned |
| 5 | User ingin bisa transfer wallet ke akun wallet yang berbeda  | Tinggi | Planned |
| 6 | User ingin bisa mengecek informasi tentang dirinya | Tinggi | Completed |
| 7 | User ingin bisa mengisi pulsa | Rendah | Unplanned |
| 8 | User ingin bisa membayar tagihan listrik | sedang | Planned |

#### Use Case Diagram
![Use Case Diagram](https://gitlab.com/kemal.mkp/bank-bjb/-/raw/main/use_case_bjb.drawio.png)


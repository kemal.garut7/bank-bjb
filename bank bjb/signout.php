<?php
session_start();
$_SESSION = [];
session_unset();
session_destroy();

// header("Location : index.php");
echo (
    "<script>
        window.alert('Kamu sudah keluar');
        window.location.href = 'index.php';
    </script>"
);
exit;

?>
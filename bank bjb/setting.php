<?php
include 'koneksi.php';
session_start();

if (!isset($_SESSION['btnPinSetting'])) {
    echo (
        "<script>
            window.alert('Halaman ini bersifat rahasia!!, lakukan konfirmasi terlebih dahulu');
            window.location.href = 'home.php';
        </script>"
    );
} else {
    // echo (
    //     "<script>
    //         window.alert('Hallo');
    //     </script>"
    // );
    $nama = $_SESSION['nama'];
    $username = $_SESSION['username'];
    $email = $_SESSION['email'];
    $alamat = $_SESSION['alamat'];
    $hp = $_SESSION['no_hp'];
    $noKartu = $_SESSION['no_kartu'];
    $saldo = $_SESSION['saldo'];    
    $saldo_rupiah = "Rp " . number_format($saldo, 0, ',', '.');


}

?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Setting</title>
    <!-- ======= Styles ====== -->
    <link rel="stylesheet" href="assets/css/style.css">

    <style>
        .profile-container {
            max-width: 800px;
            margin: 0 auto;
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            justify-content: center;
            padding-top: 20px;
            /* display: none; */
        }

        .profile {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-right: 20px;
            margin-bottom: 20px;
        }

        .profile img {
            border-radius: 50%;
            width: 150px;
            height: 150px;
        }

        .profile h3 {
            margin-top: 10px;
            font-size: 20px;
            font-weight: bold;
            text-align: center;
        }

        .settings-form {
            max-width: 500px;
            margin: 0 auto;
        }

        .settings-form h2 {
            text-align: center;
            font-size: 24px;
            margin-bottom: 20px;
        }

        .form-group {
            margin-bottom: 20px;
        }

        label {
            display: block;
            margin-bottom: 5px;
            font-weight: bold;
        }

        input[type="text"],
        input[type="email"],
        input[type="tel"],
        textarea {
            padding: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
            font-size: 16px;
            width: 100%;
            /* box-sizing: border-box; */
            background-color: #fff;
        }

        .submit-button,
        .cancel-button {
            background-color: #4CAF50;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
            margin-right: 10px;
        }

        .cancel-button {
            background-color: #f44336;
        }

        .button-container {
            text-align: center;
            margin-top:
        }
    </style>
</head>

<body>
    <!-- =============== Navigation ================ -->
    <div class="container">
        <div class="navigation">
            <ul>
                <li>
                    <a href="#">
                        <span class="icon">
                            <!-- <ion-icon name="logo-apple"></ion-icon> -->
                            <img src="assets/imgs/bjbLogo.png" style="margin-top: 20px;" alt="" width="65">
                        </span>
                        <!-- <span class="title">bjb</span> -->
                    </a>
                </li>

                <li>
                    <a href="home.php">
                        <span class="icon">
                            <ion-icon name="home"></ion-icon>
                        </span>
                        <span class="title">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="icon">
                            <ion-icon name="help"></ion-icon>
                        </span>
                        <span class="title">Bantuan</span>
                    </a>
                </li>

                <li>
                    <a href="#<?= uniqid() ?>" id="settings-link">
                        <span class="icon">
                            <ion-icon name="lock-closed"></ion-icon> </span>
                        <span class="title">Ubah Password</span>
                    </a>
                </li>

                <!-- <li>
                    <a href="#">
                        <span class="icon">
                            <ion-icon name="lock-closed"></ion-icon>
                        </span>
                        <span class="title">Password</span>
                    </a>
                </li> -->

                <!-- <li>
                    <a href="signout.php">
                        <span class="icon">
                            <ion-icon name="log-out"></ion-icon>
                        </span>
                        <span class="title">Sign Out</span>
                    </a>
                </li> -->
            </ul>
        </div>

        <!-- ========================= Main ==================== -->
        <div class="main">
            <div class="topbar">
                <div class="toggle">
                    <ion-icon name="menu-outline"></ion-icon>
                </div>
                <p class="sayNama"><b>Halo,
                        <?= $nama ?>
                    </b></p>
            </div>

            <div class="profile-container">
                <div class="profile">
                    <img src="https://i.postimg.cc/dVyNr8c8/profil.png" alt="Foto Profil">
                    <h3>
                        <i><?= $username ?></i>
                    </h3>
                    <h4 style="color: green;">
                        Saldo <?= $saldo_rupiah ?>
                    </h4>
                </div>
                <form class="settings-form">
                    <h2>Pengaturan Akun</h2>

                    <div class="form-group">
                        <label for="nama">Nama:</label>
                        <input type="text" id="nama" name="nama" value="<?= $nama ?>" disabled>
                    </div>

                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" id="email" name="email" value="<?= $email ?>" disabled>
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat:</label>
                        <textarea id="alamat" name="alamat" disabled><?= $alamat ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="no-hp">No. HP:</label>
                        <input type="tel" id="no-hp" name="no-hp" value="<?= $hp ?>" disabled>
                    </div>

                    <div class="form-group">
                        <label for="no-kartu-atm">No. Kartu ATM:</label>
                        <input type="text" id="no-kartu-atm" name="no-kartu-atm" value="<?= $noKartu ?>" disabled>
                    </div>

                    <div class="button-container">
                        <button type="submit" class="submit-button">Simpan</button>
                        <button type="button" class="cancel-button">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- =========== JS NAV =========  -->
    <script src="assets/js/main.js"></script>

    <!-- ====== ionicons ======= -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

</body>

</html>
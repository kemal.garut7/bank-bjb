<?php
include 'koneksi.php';
if (isset($_POST['btnlogin'])) {
    $pesan = '';
    $redirect = '';

    // $email = $_POST['email'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    // cek query
    // $q = $conn -> query("SELECT * FROM user WHERE email = '$email'");
    $q = $conn->query("SELECT * FROM user WHERE username = '$username'");
    $get_data = mysqli_fetch_array($q);

    //jika email ada
    if (empty($get_data)) {
        // $pesan = "Username belum terdaftar";
        $pesan = "Username tidak terdaftar";
    } else {
        //cek password
        if ($password != $get_data['password']) {
            $pesan = "username/password salah";
        } else {
            session_start();
            $_SESSION['id_user'] = $get_data['id_user'];
            $_SESSION['login'] = true;
            // $_SESSION['email'] = $email;
            $_SESSION['username'] = $username;
            $nama = $_SESSION['nama'] = $get_data['nama'];
            $_SESSION['email'] =$get_data['email'];
            $_SESSION['alamat'] = $get_data['alamat'];
            $_SESSION['no_hp'] = $get_data['no_hp'];
            $_SESSION['no_kartu'] = $get_data['no_kartu'];
            $_SESSION['saldo'] = $get_data['saldo'];
            $_SESSION['pengeluaran'] = $get_data['pengeluaran'];
            $_SESSION['pemasukan'] = $get_data['pemasukan'];
            $_SESSION['warna_kartu'] = $get_data['warna_kartu'];
            $_SESSION['password'] = $get_data['password'];


            $redirect = 'home.php';
            $pesan = "Selamat Datang $nama";
        }
    }
    echo (
        "<script>
                window.alert('$pesan');
                window.location.href = '$redirect';
            </script>"
    );
    exit;
}

?>



<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link rel="stylesheet" href="assets/css/style_login.css">

    <!-- FONT GOOGLE -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">

    <title>Login</title>
</head>

<body>

    <section class="login d-flex">
        <div class="login-left w-100 h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="kolom col-7">
                    <div class="Logo text-center">
                        <img src="assets/imgs/bjbLogo.png" alt="">
                        <!-- <img src="img/mkpLogo.png" alt=""> -->
                    </div>
                    <div class="header">
                        <h1 class="welcome">Selamat Datang...</h1>
                        <p>Hallo, <b>Login</b> untuk masuk</p>
                    </div>

                    <div class="login-form">
                        <form action="" method="post">
                            <!-- <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter your email" required autofocus> -->

                            <div id="error" style="color:red; display:none;font-style: italic;"> <ion-icon
                                    name="alert-circle-outline"></ion-icon>
                                Username harus diisi
                                tanpa spasi atau titik</div>

                            <label for="username" class="form-label">Username</label>
                            <input type="text" class="form-control" id="username" name="username"
                                placeholder="Enter your username" required autofocus autocomplete="true"
                                onkeyup="validateInput()">

                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control" id="password" name="password"
                                placeholder="Enter your password" required>

                            <a href="#" class="text-decoration-none text-center">Forgot password?</a>
                            <button class="signin" type="submit" id="btnLogin" name="btnlogin">Sign In</button>
                            <button class="signin-google">
                                <img src="assets/imgs/googleIcon.png" alt="" width="30">
                                Sign In With Google
                            </button>
                        </form>

                        <div class="NoAccount text-center">
                            <span class="d-inline">Don't have an account?
                                <a href="#" class="signup text-decoration-none d-inline">Sign up here
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
                <footer class="text-center align-items-center">
                    <span style="color: white; font-size: 13px;"><b>&copy; 2023 Muhammad Kemal Pasha. All rights
                            reserved.</b></span>
                </footer>
            </div>
        </div>

        <div class="login-right w-100 h-100">
            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="assets/imgs/bjb.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="assets/imgs/bjb2.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="assets/imgs/bjb3.jpeg" class="d-block w-100" alt="...">
                    </div>
                </div>
                <!-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button> -->
            </div>

        </div>

    </section>

    <script>
        function validateInput() {
            var input = document.getElementById("username").value;
            var btn = document.getElementById("btnLogin");
            var regex = /[\s.]/;

            if (/\s/.test(input) || /\./.test(input)) {
                document.getElementById("error").style.display = "block";
                btn.disabled = true;
                btn.style.backgroundColor = "grey";
            } else {
                document.getElementById("error").style.display = "none";
                btn.disabled = false;
                btn.style.backgroundColor = "blue";
            }
        }
    </script>


    <!-- ====== ionicons ======= -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>



    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>
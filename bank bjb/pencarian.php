<?php
include "koneksi.php";

$keyword = $_GET['keyword'];

$query_pencarian = "SELECT * FROM data
                        WHERE
                        nama LIKE '%$keyword%'OR
                        no_rekening LIKE '%$keyword%'OR
                        tanggal LIKE '%$keyword%'OR
                        nominal LIKE '%$keyword%'OR
                        status LIKE '%$keyword%'
                    ";
$cari = query($query_pencarian);


?>

<table>
    <thead>
        <tr>
            <td>No</td>
            <td>No. Rekening</td>
            <td>Bank</td>
            <td>Nama</td>
            <td>Nominal</td>
            <td>Tanggal</td>
            <td>Status</td>
        </tr>
    </thead>

    <tbody>
        <?php
        $i = 1;

        foreach ($cari as $key => $value):
            $nominal = $value['nominal'];
            $nominal_rupiah = "Rp " . number_format($nominal, 0, ',', '.');
            ?>
            <tr>
                <td>
                    <?= $i++ ?>
                </td>
                <td>
                    <?= $value['no_rekening'] ?>
                </td>
                <td>
                    <?= $value['bank'] ?>
                </td>
                <td>
                    <?= $value['nama'] ?>
                </td>
                <td>
                    <?= $nominal_rupiah ?>
                </td>
                <td style="width: 100%;">
                    <?= $value['tanggal'] ?>
                </td>
                <td><span class="status delivered">
                        <?= $value['status'] ?>
                    </span></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
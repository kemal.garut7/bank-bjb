<?php
include 'koneksi.php';
session_start();

if (!isset($_SESSION['login'])) {
    echo (
        "<script>
            window.alert('Kamu belum login');
            window.location.href = 'index.php';
        </script>"
    );
} else {


    $saldo = $_SESSION['saldo'];

    $saldo_rupiah = "Rp " . number_format($saldo, 0, ',', '.');

    $pengeluaran = $_SESSION['pengeluaran'];
    // $pengeluaran += 10000;
    $pengeluaran_rupiah = "Rp " . number_format($pengeluaran, 0, ',', '.');

    $pemasukan = $_SESSION['pemasukan'];
    $pemasukan_rupiah = "Rp " . number_format($pemasukan, 0, ',', '.');
    $hasil_pengeluaran = $_SESSION['saldo'] - $_SESSION['pengeluaran'];
    $hasil_pengeluaran = "Rp " . number_format($hasil_pengeluaran, 0, ',', '.');

    $warnaKartu = $_SESSION['warna_kartu'];
    $nama = $_SESSION['nama'];

    $id_user = $_SESSION['id_user'];
    $password = $_SESSION['password'];


    $query = query("SELECT * FROM data WHERE id_user='$id_user' ORDER BY id_data ASC");

    if (isset($_POST['cari'])) {
        $query = cari($_POST['keyword']);
    }


    if (isset($_POST['submit-transfer'])) {
        $nominal_input = $_POST['nominal'];

        if (transferBank($_POST) > 0) {
            // saat transfer berhasil, maka saldo berkurang dan pengeluaran bertambah
            $saldo -= $nominal_input; // aritmatika
            $pengeluaran += $nominal_input; // aritmatika
            $updateSaldo = "UPDATE user SET saldo = '$saldo', pengeluaran = '$pengeluaran' WHERE id_user = '$id_user'";

            echo "
                    <script>
                        alert('Transfer berhasil, saldo dikurangi Rp $nominal_input menjadi $saldo');
                        document.location.href = 'home.php';
                    </script>
                ";
        } else {
            echo "
                    <script>
                        alert('Transfer gagal');
                    </script>
                ";
        }
        mysqli_query($conn, $updateSaldo);

    }

}

?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <!-- ======= Styles ====== -->
    <link rel="stylesheet" href="assets/css/style.css">

    <style>
        .profile-container {
            max-width: 800px;
            margin: 0 auto;
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            justify-content: center;
            padding-top: 20px;
            display: none;
        }

        .profile {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-right: 20px;
            margin-bottom: 20px;
        }

        .profile img {
            border-radius: 50%;
            width: 150px;
            height: 150px;
        }

        .profile h3 {
            margin-top: 10px;
            font-size: 20px;
            font-weight: bold;
            text-align: center;
        }

        .settings-form {
            max-width: 500px;
            margin: 0 auto;
        }

        .settings-form h2 {
            text-align: center;
            font-size: 24px;
            margin-bottom: 20px;
        }

        .form-group {
            margin-bottom: 20px;
        }

        label {
            display: block;
            margin-bottom: 5px;
            font-weight: bold;
        }

        input[type="text"],
        input[type="email"],
        input[type="tel"],
        textarea {
            padding: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
            font-size: 16px;
            width: 100%;
            box-sizing: border-box;
            background-color: #fff;
        }

        .submit-button,
        .cancel-button {
            background-color: #4CAF50;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
            margin-right: 10px;
        }

        .cancel-button {
            background-color: #f44336;
        }

        .button-container {
            text-align: center;
            margin-top:
        }






        /* CSS */
        .modal {
            display: none;
            position: fixed;
            z-index: 1;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.5);
        }

        .modal-content {
            background-color: #fefefe;
            margin: 15% auto;
            padding: 20px;
            border: 1px solid #888;
            width: 40%;
            text-align: center;
            border-radius: 10px;
        }

        .modal-content h4 {
            margin-top: 0;
        }
    </style>
</head>

<body>
    <div class="container">

        <!-- =============== Navigation ================ -->
        <div class="navigation">
            <ul>
                <li>
                    <a href="#">
                        <span class="icon">
                            <!-- <ion-icon name="logo-apple"></ion-icon> -->
                            <img src="assets/imgs/bjbLogo.png" style="margin-top: 20px;" alt="" width="65">
                        </span>
                        <!-- <span class="title">bjb</span> -->
                    </a>
                </li>

                <li>
                    <a href="home.php">
                        <span class="icon">
                            <ion-icon name="home"></ion-icon>
                        </span>
                        <span class="title">Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="#TransferBank" id="modal-link">
                        <span class="icon">
                            <ion-icon name="cash"></ion-icon>
                        </span>
                        <span class="title">Transfer Bank</span>
                    </a>
                </li>

                <li>
                    <a href="#wallet" id="modal-link-wallet">
                        <span class="icon">
                            <ion-icon name="wallet"></ion-icon>
                        </span>
                        <span class="title">E-Wallet</span>
                    </a>
                </li>

                <li>
                    <a href="#">
                        <span class="icon">
                            <ion-icon name="help"></ion-icon>
                        </span>
                        <span class="title">Bantuan</span>
                    </a>
                </li>

                <li>
                    <a href="setting.php" id="settings-link">
                        <span class="icon">
                            <ion-icon name="settings"></ion-icon>
                        </span>
                        <span class="title">Pengaturan</span>
                    </a>
                </li>

                <!-- <li>
                    <a href="#">
                        <span class="icon">
                            <ion-icon name="lock-closed"></ion-icon>
                        </span>
                        <span class="title">Password</span>
                    </a>
                </li> -->

                <li>
                    <a href="signout.php" onclick="return confirm('Yakin ingin keluar ?')">
                        <span class="icon">
                            <ion-icon name="log-out" style="color: rgba(147, 0, 0, 0.628);"></ion-icon>
                        </span>
                        <span class="title" style="color: rgba(147, 0, 0, 0.628);"><b>Keluar</b></span>
                    </a>
                </li>
            </ul>
        </div>

        <!-- ========================= Main ==================== -->
        <div class="main">
            <div class="topbar">
                <div class="toggle">
                    <ion-icon name="menu-outline"></ion-icon>
                </div>

                <!-- <div class="search">
                    <label>
                        <input type="text" placeholder="Search here">
                        <ion-icon name="search-outline"></ion-icon>
                    </label>
                </div> -->

                <p class="sayNama"><b>Halo,
                        <?= $nama ?>
                    </b></p>

                <!-- <div class="user">
                    <p>Muhammad Kemal Pasha</p>
                </div> -->
            </div>

            <div class="middleBar">
                <!-- ======================= Cards ================== -->
                <div class="cardBox">
                    <div class="card">
                        <div>
                            <div class="text">Saldo</div>
                            <div class="cardName">
                                <?= $saldo_rupiah ?>
                            </div>
                        </div>

                        <div class="iconDompet">
                            <ion-icon name="wallet"></ion-icon>
                        </div>
                    </div>

                    <div class="card status">
                        <div>
                            <div class="text">Warna Kartu</div>
                            <div class="cardName cardNameStatus"><b>
                                    <?= $warnaKartu ?>
                                </b></div>
                        </div>

                        <div class="<?= $warnaKartu ?>">
                            <ion-icon name="card"></ion-icon>
                        </div>
                    </div>

                    <div class="card">
                        <div>
                            <div class="text">Pemasukan</div>
                            <div class="cardName">
                                <?= $pemasukan_rupiah ?>
                            </div>
                        </div>

                        <div class="iconPemasukan">
                            <ion-icon name="arrow-down-outline"></ion-icon>
                        </div>
                    </div>

                    <div class="card">
                        <div>
                            <div class="text">Pengeluaran</div>
                            <div class="cardName">
                                <?= $pengeluaran_rupiah ?>
                            </div>
                        </div>

                        <div class="iconPengeluaran">
                            <ion-icon name="arrow-up-outline"></ion-icon>
                        </div>
                    </div>
                </div>

                <!-- ================ history ================= -->
                <div class="history">
                    <div class="recentOrders">
                        <div class="cardHeader">
                            <h2>History Transaksi</h2>
                            <form action="" method="post" class="search-form">
                                <input type="text" name="keyword" id="input-cari" autocomplete="off"
                                    placeholder="Cari transaksi...">
                                <!-- <button name="cari" type="submit" id="btn-cari" onclick="checkInput()" >Cari</button> -->
                            </form>
                            <!-- <a href="#" class="btn">View All</a> -->
                        </div>

                        <div id="recentOrders">
                            <table>
                                <thead>
                                    <tr>
                                        <td>No</td>
                                        <td>No. Rekening</td>
                                        <td>Bank</td>
                                        <td>Nama</td>
                                        <td>Nominal</td>
                                        <td>Tanggal</td>
                                        <td>Status</td>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $i = 1;

                                    foreach ($query as $key => $value):
                                        $nominal = $value['nominal'];
                                        $nominal_rupiah = "Rp " . number_format($nominal, 0, ',', '.');
                                        ?>
                                        <tr>
                                            <td>
                                                <?= $i++ ?>
                                            </td>
                                            <td>
                                                <?= $value['no_rekening'] ?>
                                            </td>
                                            <td>
                                                <?= $value['bank'] ?>
                                            </td>
                                            <td>
                                                <?= $value['nama'] ?>
                                            </td>
                                            <td>
                                                <?= $nominal_rupiah ?>
                                            </td>
                                            <td style="width: 100%;">
                                                <?= $value['tanggal'] ?>
                                            </td>
                                            <td><span class="status <?= $value['status'] ?>">
                                                    <?= $value['status'] ?>
                                                </span></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



    <!-- The Modal setting validasi -->
    <div id="pin-modal" class="modal modal2">
        <?php
        if (isset($_POST['submitPin'])) {
            if ($_POST['password'] === $password) {
                $_SESSION['btnPinSetting'] = true;
                echo ("
                    <script>
                        alert('Hallo $nama');
                        window.location.href = 'setting.php';
                    </script>
                ");
            } else {
                echo ("
                    <script>
                        alert('Password salah !');
                    </script>
                ");
            }
        }
        ?>
        <div class="modal-content-setting">
            <span class="closebtn">&times;</span>
            <h2 style="margin-bottom: 20px;">Data Privasi</h2>
            <p style="font-family: Arial, Helvetica, sans-serif; margin-bottom: 5px; font-size: 14px;">Konfirmasi bahwa
                kamu,
                <b>
                    <?= $nama ?>
                </b>
            </p>

            <form action="" method="post">
                <label for="nomor-rekening">Masukan Password:</label>
                <input type="password" id="pin-input" name="password" autocomplete="off" autofocus required>
                <button class="btnTransfer" type="submit" name="submitPin" id="submit-pin">Submit</button>
            </form>
        </div>
    </div>

    <!-- The Modal Transfer Bank -->
    <div id="modal-bank" class="modal">
        <?php


        ?>
        <!-- Modal content Transfer Bank -->
        <div class="modal-content-trasnfer-bank">
            <span class="close">&times;</span>
            <h2 style="margin-bottom: 20px;">Transfer Bank</h2>
            <form action="" method="POST" onsubmit="return validateForm()">
                <label for="bank">Bank:</label>
                <select id="bank" name="bank" required>
                    <option value="">-- Pilih Bank --</option>
                    <option value="BJB">BJB</option>
                    <!-- <option value="BNI">BNI</option> -->
                    <option value="BRI">BRI</option>
                    <option value="MANDIRI">Mandiri</option>
                    <!-- <option value="JAGO">Jago</option> -->
                </select>

                <div id="warning" style="color:red"></div>

                <label for="nomor-rekening">Nomor Rekening:</label>
                <input type="text" id="nomor-rekening" name="nomor_rekening" autocomplete="off" disabled>


                <label for="nama-rekening">Pemilik Rekening:</label>
                <input type="text" id="nama-rekening" name="nama_rekening" readonly required>

                <label for="amount">Nominal:</label>
                <input type="text" id="amount" name="nominal" required>

                <input type="hidden" name="tanggal" value="<?php echo date('Y-m-d'); ?>">

                <button class="btnTransfer" type="submit" id="submit-btn" name="submit-transfer"
                    disabled>Transfer</button>
            </form>
        </div>
    </div>

    <!-- The Modal E-wallet -->
    <div id="modal-wallet" class="modal Modal_Wallet">
        <!-- Modal content Ewallet -->
        <div class="modal-content-trasnfer-bank modal-content-Ewallet">
            <span class="close closeWallet">&times;</span>
            <h2 style="margin-bottom: 20px;">E-Wallet</h2>
            <form action="" method="POST" onsubmit="return validateForm()">
                <label for="bank">E-Wallet:</label>
                <select id="bank" name="bank" required>
                    <option value="">-- Pilih E-Wallet --</option>
                    <option value="DANA">Dana</option>
                    <!-- <option value="BNI">BNI</option> -->
                    <option value="GOPAY">Gopay</option>
                    <option value="OVO">Ovo</option>
                </select>

                <div id="warning" style="color:red"></div>

                <label for="nomor-wallet">Nomor Wallet:</label>
                <input type="text" id="nomor-wallet" name="nomor_wallet" autocomplete="off">


                <label for="nama-wallet">Pemilik E-Wallet:</label>
                <input type="text" id="nama-wallet" name="nama_wallet" disabled required>

                <label for="amount">Nominal:</label>
                <input type="text" id="amount" name="nominal" required>

                <input type="hidden" name="tanggal" value="<?php echo date('Y-m-d'); ?>">

                <button class="btnTransfer" type="submit" id="submit-btn" name="submit" disabled>Kirim</button>
            </form>
        </div>
    </div>





    <!-- =========================================================JAVASCRIPT OOP========================================================= -->

    <!-- live search dengan ajax (overload)-->
    <script>
        class PencarianHistory {
            constructor(url, method) {
                this.url = url;
                this.method = method;
            }

            kirimData() {
                // objek ajax
                var xhr = new XMLHttpRequest();

                // cek ajax
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        recentOrders.innerHTML = xhr.responseText;
                    }
                }

                // eksekusi ajax
                xhr.open(this.method, this.url, true);
                xhr.send();
            }

            // Overload
            kirimData(data) {
                // objek ajax
                var xhr = new XMLHttpRequest();

                // cek ajax
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        recentOrders.innerHTML = xhr.responseText;
                    }
                }

                // eksekusi ajax dengan data
                xhr.open(this.method, this.url, true);
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.send(JSON.stringify(data));
            }
        }

        var inputCari = document.getElementById('input-cari');
        var recentOrders = document.getElementById('recentOrders');

        inputCari.addEventListener('keyup', function () {
            var url = 'pencarian.php?keyword=' + inputCari.value;

            // objek request dengan URL sebagai parameter
            var getRequest = new PencarianHistory(url, 'GET');

            // Memanggil method kirimData() tanpa parameter
            getRequest.kirimData();

            // Membuat objek request dengan URL dan data sebagai parameter
            var postRequest = new PencarianHistory(url, 'POST');

            // Memanggil method kirimData() dengan parameter data
            postRequest.kirimData({ keyword: inputCari.value });
        });

    </script>


    <!-- cek nama rekening kosong -->
    <script>
        function validateForm() {
            const nameInput = document.getElementById("nama-rekening");
            if (nameInput.value == "") {
                alert("Pemilik rekening tidak ditemukan");
                return false;
            }
            return true;
        }
    </script>

    <!-- logic transfer bank dan setting -->
    <script>
        // Abstract class BankInput
        class BankInput {
            constructor() {
                // keyword abstract
                if (new.target === BankInput) {
                    throw new TypeError("Cannot construct abstract class");
                }

                // penggunaan " _" berarti private
                this._bankInput = document.getElementById("bank");
                this._rekeningInput = document.getElementById("nomor-rekening");
                this._warning = document.getElementById("warning");
                this._submitButton = document.getElementById("submit-btn");

                this._bankInput.addEventListener("change", () => {
                    this.listBank();
                });

                this._rekeningInput.addEventListener("input", () => {
                    this.minRekening();
                });

                this._submitButton.addEventListener("click", () => {
                    this.btnSubmit();
                });
            }

            // Abstract method
            listBank() {
                throw new Error("Method 'listBank()' must be implemented.");
            }

            // Abstract method
            minRekening() {
                throw new Error("Method 'minRekening()' must be implemented.");
            }

            // Abstract method
            btnSubmit() {
                throw new Error("Method 'btnSubmit()' must be implemented.");
            }
        }

        class BankInputPilih extends BankInput {
            // method abstract di panggil
            listBank() {
                const bankCodeMap = {
                    BJB: "098",
                    BRI: "099",
                    MANDIRI: "000",
                };
                const bank = this._bankInput.value;
                const bankCode = bankCodeMap[bank];
                if (bankCode) {
                    this._rekeningInput.value = bankCode;
                    this._rekeningInput.disabled = false;
                    this._warning.innerHTML = "";
                } else {
                    this._rekeningInput.value = "";
                    this._rekeningInput.disabled = true;
                    this._warning.innerHTML = "Mohon pilih Bank untuk melanjutkan";
                }
            }
            // method abstract di panggil
            minRekening() {
                const minLength = 6;
                const rekeningInput = this._rekeningInput.value;
                const bank = this._bankInput.value;
                const bankCodeMap = {
                    BJB: "098",
                    BRI: "099",
                    MANDIRI: "000",
                };
                const bankCode = bankCodeMap[bank];
                if (rekeningInput.length < minLength) {
                    this._warning.innerHTML = `Nomor rekening minimal ${minLength} digit`;
                    this._submitButton.disabled = true;
                } else if (rekeningInput.substr(0, 3) !== bankCode) {
                    this._rekeningInput.value = bankCode + rekeningInput.substr(3);
                } else {
                    this._warning.innerHTML = "";
                    this._submitButton.disabled = false;
                }
            }

            // method abstract di panggil
            btnSubmit() {
                const minLength = 6;
                const rekeningInput = this._rekeningInput.value;
                if (rekeningInput.length < minLength) {
                    this._warning.innerHTML = `Nomor rekening minimal ${minLength} digit`;
                    return false;
                }
                return true;
            }
        }

        const bankInput = new BankInputPilih();






        // Modal Pin Setting
        class settingModal {
            constructor(modalLink, modalId, pinId, buttonId, close) {
                this.settingsLink = document.getElementById(modalLink);
                this.pinModal = document.getElementById(modalId);
                this._pinInput = document.getElementById(pinId);
                this.submitPinButton = document.getElementById(buttonId);
                this.closeButton = document.querySelector(`.${close}`);

                this.settingsLink.addEventListener("click", this.tampilModal.bind(this));
                this.submitPinButton.addEventListener("click", this.submitPin.bind(this));
                this.closeButton.addEventListener("click", this.sembunyiModal.bind(this));
                window.addEventListener("click", this.sembunyiModalWindow.bind(this));
            }

            tampilModal(event) {
                event.preventDefault(); //mencegah event menavigasikan pengguna ke halaman
                this.pinModal.style.display = "block";
            }

            sembunyiModal() {
                this.pinModal.style.display = "none";
            }

            sembunyiModalWindow(event) {
                if (event.target === this.pinModal) {
                    this.sembunyiModal();
                }
            }

            submitPin() {

            }
        }

        const pinModal = new settingModal("settings-link", "pin-modal", "pin-input", "submit-pin", "closebtn");



        // penyamaan no rekening dengan database
        class DataRekening {
            constructor() {
                this._nomorRekeningInput = document.getElementById('nomor-rekening');
                this._namaRekeningInput = document.getElementById('nama-rekening');

                this._nomorRekeningInput.addEventListener('input', this.nomorRekening.bind(this));
            }

            get nomorRekeningInput() {
                return this._nomorRekeningInput;
            }

            set nomorRekeningInput(value) {
                this._nomorRekeningInput = value;
            }

            get namaRekeningInput() {
                return this._namaRekeningInput;
            }

            set namaRekeningInput(value) {
                this._namaRekeningInput = value;
            }

            nomorRekening() {
                const nomorRekening = this.nomorRekeningInput.value;
                const xhr = new XMLHttpRequest();
                xhr.open('GET', `koneksi.php?nomor_rekening=${nomorRekening}`);
                xhr.onload = () => {
                    if (xhr.status === 200) {
                        const namaRekening = xhr.responseText;
                        this.namaRekeningInput.value = namaRekening;
                    } else {
                        console.error('Gagal memuat data');
                    }
                };
                xhr.send();
            }
        }




        class Wallet extends DataRekening {
            constructor() {
                super();
                this.nomorRekeningInput = document.getElementById('nomor-wallet');
                this.namaRekeningInput = document.getElementById('nama-wallet');

                this.nomorRekeningInput.addEventListener('input', this.nomorRekening.bind(this));
            }

            // polymorphism override
            nomorRekening() {
                super.nomorRekening(); // mewarisi nomor rekening bank

                // mengambil data no_hp dari database
                const nomorWallet = this.nomorRekeningInput.value;
                const xhr = new XMLHttpRequest();
                xhr.open('GET', `koneksi.php?nomor_wallet=${nomorWallet}`);
                xhr.onload = () => {
                    if (xhr.status === 200) {
                        const namaRekening = xhr.responseText;
                        this.namaRekeningInput.value = namaRekening;
                    } else {
                        console.error('Gagal memuat data');
                    }
                }
                xhr.send();
            }
        }

        const dataRekening = new DataRekening();
        const wallet = new Wallet('nomor_wallet', 'nama_wallet');

    </script>

    <!-- MODAL transfer bank dan e-wallet  -->
    <script>
        class Modal {
            constructor(modalId, linkId, closeButtonClass) {
                this.modal = document.getElementById(modalId);
                this.link = document.getElementById(linkId);
                this.closeButton = document.querySelector(`.${closeButtonClass}`);
            }

            open() {
                this.modal.style.display = "block";
            }

            close() {
                this.modal.style.display = "none";
            }

            init() {
                this.link.addEventListener("click", this.open.bind(this));
                this.closeButton.addEventListener("click", this.close.bind(this));
                window.addEventListener("click", (event) => {
                    if (event.target === this.modal) {
                        this.close();
                    }
                });
            }
        }

        // modal Transfer Bank 
        const myModal = new Modal("modal-bank", "modal-link", "close");
        myModal.init();

        // modal Ewallet        
        const ModalWallet = new Modal("modal-wallet", "modal-link-wallet", "closeWallet");
        ModalWallet.init();
    </script>



    <!-- =========== JS =========  -->
    <script src="assets/js/main.js"></script>
    <!-- ====== ionicons ======= -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

</body>

</html>